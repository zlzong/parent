package com.changgou.web.order.controller;

import com.alibaba.fastjson.JSON;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.order.feign.CartFeign;
import com.changgou.order.feign.OrderFeign;
import com.changgou.order.pojo.Order;
import com.changgou.order.pojo.OrderItem;
import com.changgou.user.feign.AddressFeign;
import com.changgou.user.pojo.Address;
import com.changgou.web.order.util.HttpUtils;
import com.changgou.web.order.util.OssUtil;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Controller
@RequestMapping("/worder")
public class OrderController {

    @Autowired
    private AddressFeign addressFeign;

    @Autowired
    private CartFeign cartFeign;


    @RequestMapping("/ready/order")
    public String readyOrder(Model model){
        //收件人的地址信息
        List<Address> addressList = addressFeign.list().getData();
        model.addAttribute("address",addressList);

        //购物车信息
        Map map = cartFeign.list();
        List<OrderItem> orderItemList = (List<OrderItem>) map.get("orderItemList");
        Integer totalMoney = (Integer) map.get("totalMoney");
        Integer totalNum = (Integer) map.get("totalNum");

        model.addAttribute("carts",orderItemList);
        model.addAttribute("totalMoney",totalMoney);
        model.addAttribute("totalNum",totalNum);

        //默认收件人信息
        for (Address address : addressList) {
            if ("1".equals(address.getIsDefault())){
                //默认收件人
                model.addAttribute("deAddr",address);
                break;
            }
        }
        return "order";
    }

    @Autowired
    private OrderFeign orderFeign;

    @PostMapping("/add")
    @ResponseBody
    public Result add(@RequestBody Order order){
        Result result = orderFeign.add(order);
        return result;
    }

    @GetMapping("/toPayPage")
    public String toPayPage(String orderId,Model model){
        //获取到订单的相关信息
        Order order = orderFeign.findById(orderId).getData();
        model.addAttribute("orderId",orderId);
        model.addAttribute("payMoney",order.getPayMoney());
        return "pay";
    }

    /**
     * @author zlzong
     */
    @GetMapping("/orderExpress")
    public String orderExpress(String orderId,Model model){
        //获取物流单号
        Order order = orderFeign.findById(orderId).getData();
        String shippingName = order.getShippingName();
        String shippingCode = order.getShippingCode();
        Map exStatusMap = this.exStatus(shippingName, shippingCode);
        exStatusMap.put("shippingCode",shippingCode);
        model.addAttribute("exStatus",exStatusMap);
        model.addAttribute("orderId",orderId);
        return "orderExpress";
    }

    /**
     * @author zlzong
     * 把评价传到mongodb
     */
    @ResponseBody
    @GetMapping("/skuComment/{skuId}/{remark}/{comment}/{orderId}")
    public Result skuComment(@PathVariable("skuId") String skuId,@PathVariable("remark") Integer remark,@PathVariable("comment") String comment,@PathVariable("orderId") String orderId) {
        if (comment.length()>=6 && comment.length()<=500) {
            orderFeign.skuComment(skuId,remark,comment);
            return new Result(true,StatusCode.OK,"评论发布成功");
        } else {
            return new Result(false,StatusCode.ERROR,"评论内容长度请控制在6～500之间");
        }
    }

    @ResponseBody
    @PostMapping("/uploadImage")
    public Result uploadImage(MultipartFile file) {
        OssUtil ossUtil = new OssUtil();
        String uploadStatus = ossUtil.uploadImg2Oss(file);
        return new Result(true,StatusCode.OK,uploadStatus);
    }

    public Map exStatus(String shippingName,String shippingCode) {
        String host = "https://wuliu.market.alicloudapi.com";
        String path = "/kdi";
        String method = "GET";
        String appcode = "5558b049fe3845ad88f5b3bd2319a4ca";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("no", shippingCode);
        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            String s = EntityUtils.toString(response.getEntity());
            Map map = JSON.parseObject(s, Map.class);
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}