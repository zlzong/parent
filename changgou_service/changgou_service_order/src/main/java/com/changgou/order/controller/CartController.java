package com.changgou.order.controller;

import com.changgou.entity.Result;
import com.changgou.entity.StatusCode;
import com.changgou.order.config.TokenDecode;
import com.changgou.order.service.CartService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/cart")
public class CartController {
    @Autowired
    private CartService cartService;

    @Autowired
    private TokenDecode tokenDecode;

    @GetMapping("/add")
    public Result add(@RequestParam("skuId") String skuId, @RequestParam("num") Integer num) {
        String username = tokenDecode.getUserInfo().get("username");
        cartService.add(skuId,num,username);
        return new Result(true, StatusCode.OK,"加入购物车成功");
    }

    /**
     * 查询用户购物车列表
     */
    @GetMapping("/list")
    public Map list() {
        String username = tokenDecode.getUserInfo().get("username");
        Map map = cartService.list(username);
        map.put("username",username);
        return map;
    }

    /**
     * 更新购物车列表
     * @param skuId
     * @param num
     * @return
     */
    @GetMapping("/update")
    public Result update(@RequestParam("skuId") String skuId, @RequestParam("num") Integer num) {
        String username = tokenDecode.getUserInfo().get("username");
        cartService.update(skuId,num,username);
        return new Result(true, StatusCode.OK,"加入购物车成功");
    }
}