package com.changgou.order.service;

import com.changgou.order.pojo.OrderItem;

import java.util.List;
import java.util.Map;

public interface CartService {
    void add(String skuId, Integer num, String username);

    Map list(String username);

    void update(String skuId, Integer num, String username);

    List<OrderItem> findCartList(String username);
}